<?

foreach ($arResult["SECTIONS"] as $arSection) {
	if (!empty($arSection["PICTURE"]['SRC'])) {
	    $sectImg = $arSection["PICTURE"]['SRC'];
	} else {
	    $elWithPicture = CIBlockElement::GetList(
		        array("ID" => "ASC"),
		        array(
		            "IBLOCK_ID" => $arSection['IBLOCK_ID'],
		            "SECTION_ID" => $arSection['ID'],
		            "!DETAIL_PICTURE" => false
		        ),
		        false,
		        array('nTopCount' => 1),
		        array('DETAIL_PICTURE')
	        );
	    while ($arPict = $elWithPicture->Fetch()) {
	        $renderImage2 = CFile::ResizeImageGet(
	           	$arPict["DETAIL_PICTURE"], array("width" => 200, "height" => 200), BX_RESIZE_IMAGE_EXACT, false
	        );
	        $sectImg = $renderImage2['src'];
	    }
	    if (empty($sectImg)) {
	        $sectImg = '/local/img/nophoto.jpg';
	    }
	}

	echo $sectImg; // Тут уже верстка
}

?>